# Gitlab CI Scheduled Rebase

This snippet contains a `rebase` job that rebases the branch where `.gitlab-ci.yml` is onto `master` and update the former branch. The recommended use case is serving as CI for a repo outside GitLab.

## Prerequesite

- The project is a mirror of the upstream repo (such that `origin/master` -> `upstream/master`)
- The branch where `.gitlab-ci.yml` is should be branched from `origin/master`
- The modification is small and git is able to rebase without human intervention
- `CI_GIT_TOKEN` custom environment variable with `repo:write` permission

## Useage

1. Extend it.
    ````yaml
    include: 'https://gitlab.com/proletarius101/gitlab-ci-templates/-/raw/master/scheduled_rebase.yml'

    compile:
        stage: build
        script: 
        - echo "hello world"
    ````
2. Set a [schedule](https://gitlab.com/help/ci/pipelines/schedules).